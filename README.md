# UU INFOMVIS - Information Visualization

This repository holds the UU INFOMVIS - Information Visualization website.

- The 2023 website is hosted in the repository "2023" and automatically built by jekyll on Gitlab
- The 2022 website is hosted in the repository "2022" and automatically built by jekyll on Gitlab
- The index.html file automatically redirects to the latest year.

## Last Changes

- November 2023: Changed to 2023 website

# Editor

Michael Behrisch, [https://michael.behrisch.info](https://michael.behrisch.info)


